# you can build a new gem with bundle gem, or do so the more minimal way with a gemspec file like this: 
Gem::Specification.new do |spec|
  spec.name          = "microframework"
  spec.version       = "0.0.1"
  spec.authors       = ["Missy Davies"]
  spec.summary       = %q{A tiny Ruby web framework similar to sinatra},
  spec.license       = "MIT"
  spec.files = Dir["**/*.rb"]
  spec.add_runtime_dependency "rack", "~>2.0.7"
end