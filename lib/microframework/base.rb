module Microframework; end # Empty module 

module Microframework::Methods
  # def get(spec, &block) # spec = /hello for a hello route, basicalyl a match for a route
  #   @routes ||= []
  #   @routes.push({
  #     spec: spec, 
  #     method: :get, 
  #     handler: block
  #   })
  # end

  [:get, :post, :head, :put].each do |http_method|
    define_method(http_method) do |spec, &block|
      @routes ||= []
      @routes.push({
        spec: spec,
        method: http_method,
        handler: block
      })
    end
  end

  def call(env) # uses routes array above 
    method = env['REQUEST_METHOD'].downcase.to_sym # get the method out of the env hash as this contains all the info about the request, this would give us something like :get 
    @routes.each do |entry|
      next if method != entry[:method]
      if env['PATH_INFO'][entry[:spec]]
        return [ 200, { 'Content-Type' => 'text/html' }, # rack array
                [ entry[:handler].call(env) ] ]
      end
    end
    [ 404, {}, ["Route not found"] ]
  end
end

class Microframework::App 
  # This gives you a parent class to inherit from which will allow you to have an app object that accepts your syntax
  extend Microframework::Methods 
end