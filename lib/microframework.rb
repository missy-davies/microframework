puts "Yes, we required this file."

MAIN = self # Capture Ruby's Main object in a constant 

require_relative "microframework/base"

# take all methods from that module, then make them available on the main instance (main is the default top level object in Ruby)
MAIN.extend Microframework::Methods 
