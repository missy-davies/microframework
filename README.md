Working through [Rebuilding Rails microframework](https://rebuilding-rails.com/) video chapter to build a simple, bare bones Sinatra-like framework:
- [Microframework repo](https://gitlab.com/missy-davies/microframework)
- [Modular app repo](https://gitlab.com/missy-davies/modular-app)
- [DSL app repo](https://gitlab.com/missy-davies/dsl-app) 

To run:
1. Clone all repositories
2. cd into either the modular or DSL app and run `bundle`
3. run `bundle exec rackup -p 3000` to run app on `localhost:3000/hello`
